/**
  @Generated Pin Manager Header File

  @Company:
    Microchip Technology Inc.

  @File Name:
    pin_manager.h

  @Summary:
    This is the Pin Manager file generated using MPLAB(c) Code Configurator

  @Description:
    This header file provides implementations for pin APIs for all pins selected in the GUI.
    Generation Information :
        Product Revision  :  MPLAB(c) Code Configurator - 4.26
        Device            :  PIC18F24K40
        Version           :  1.01
    The generated drivers are tested against the following:
        Compiler          :  XC8 1.35
        MPLAB             :  MPLAB X 3.40

    Copyright (c) 2013 - 2015 released Microchip Technology Inc.  All rights reserved.

    Microchip licenses to you the right to use, modify, copy and distribute
    Software only when embedded on a Microchip microcontroller or digital signal
    controller that is integrated into your product or third party product
    (pursuant to the sublicense terms in the accompanying license agreement).

    You should refer to the license agreement accompanying this Software for
    additional information regarding your rights and obligations.

    SOFTWARE AND DOCUMENTATION ARE PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND,
    EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION, ANY WARRANTY OF
    MERCHANTABILITY, TITLE, NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
    IN NO EVENT SHALL MICROCHIP OR ITS LICENSORS BE LIABLE OR OBLIGATED UNDER
    CONTRACT, NEGLIGENCE, STRICT LIABILITY, CONTRIBUTION, BREACH OF WARRANTY, OR
    OTHER LEGAL EQUITABLE THEORY ANY DIRECT OR INDIRECT DAMAGES OR EXPENSES
    INCLUDING BUT NOT LIMITED TO ANY INCIDENTAL, SPECIAL, INDIRECT, PUNITIVE OR
    CONSEQUENTIAL DAMAGES, LOST PROFITS OR LOST DATA, COST OF PROCUREMENT OF
    SUBSTITUTE GOODS, TECHNOLOGY, SERVICES, OR ANY CLAIMS BY THIRD PARTIES
    (INCLUDING BUT NOT LIMITED TO ANY DEFENSE THEREOF), OR OTHER SIMILAR COSTS.

*/


#ifndef PIN_MANAGER_H
#define PIN_MANAGER_H

#define INPUT   1
#define OUTPUT  0

#define HIGH    1
#define LOW     0

#define ANALOG      1
#define DIGITAL     0

#define PULL_UP_ENABLED      1
#define PULL_UP_DISABLED     0

// get/set Pot aliases
#define Pot_TRIS               TRISAbits.TRISA0
#define Pot_LAT                LATAbits.LATA0
#define Pot_PORT               PORTAbits.RA0
#define Pot_WPU                WPUAbits.WPUA0
#define Pot_OD                ODCONAbits.ODCA0
#define Pot_ANS                ANSELAbits.ANSELA0
#define Pot_SetHigh()            do { LATAbits.LATA0 = 1; } while(0)
#define Pot_SetLow()             do { LATAbits.LATA0 = 0; } while(0)
#define Pot_Toggle()             do { LATAbits.LATA0 = ~LATAbits.LATA0; } while(0)
#define Pot_GetValue()           PORTAbits.RA0
#define Pot_SetDigitalInput()    do { TRISAbits.TRISA0 = 1; } while(0)
#define Pot_SetDigitalOutput()   do { TRISAbits.TRISA0 = 0; } while(0)
#define Pot_SetPullup()      do { WPUAbits.WPUA0 = 1; } while(0)
#define Pot_ResetPullup()    do { WPUAbits.WPUA0 = 0; } while(0)
#define Pot_SetPushPull()    do { ODCONAbits.ODCA0 = 1; } while(0)
#define Pot_SetOpenDrain()   do { ODCONAbits.ODCA0 = 0; } while(0)
#define Pot_SetAnalogMode()  do { ANSELAbits.ANSELA0 = 1; } while(0)
#define Pot_SetDigitalMode() do { ANSELAbits.ANSELA0 = 0; } while(0)

// get/set RA2 procedures
#define RA2_SetHigh()    do { LATAbits.LATA2 = 1; } while(0)
#define RA2_SetLow()   do { LATAbits.LATA2 = 0; } while(0)
#define RA2_Toggle()   do { LATAbits.LATA2 = ~LATAbits.LATA2; } while(0)
#define RA2_GetValue()         PORTAbits.RA2
#define RA2_SetDigitalInput()   do { TRISAbits.TRISA2 = 1; } while(0)
#define RA2_SetDigitalOutput()  do { TRISAbits.TRISA2 = 0; } while(0)
#define RA2_SetPullup()     do { WPUAbits.WPUA2 = 1; } while(0)
#define RA2_ResetPullup()   do { WPUAbits.WPUA2 = 0; } while(0)
#define RA2_SetAnalogMode() do { ANSELAbits.ANSELA2 = 1; } while(0)
#define RA2_SetDigitalMode()do { ANSELAbits.ANSELA2 = 0; } while(0)

// get/set D4 aliases
#define D4_TRIS               TRISAbits.TRISA6
#define D4_LAT                LATAbits.LATA6
#define D4_PORT               PORTAbits.RA6
#define D4_WPU                WPUAbits.WPUA6
#define D4_OD                ODCONAbits.ODCA6
#define D4_ANS                ANSELAbits.ANSELA6
#define D4_SetHigh()            do { LATAbits.LATA6 = 1; } while(0)
#define D4_SetLow()             do { LATAbits.LATA6 = 0; } while(0)
#define D4_Toggle()             do { LATAbits.LATA6 = ~LATAbits.LATA6; } while(0)
#define D4_GetValue()           PORTAbits.RA6
#define D4_SetDigitalInput()    do { TRISAbits.TRISA6 = 1; } while(0)
#define D4_SetDigitalOutput()   do { TRISAbits.TRISA6 = 0; } while(0)
#define D4_SetPullup()      do { WPUAbits.WPUA6 = 1; } while(0)
#define D4_ResetPullup()    do { WPUAbits.WPUA6 = 0; } while(0)
#define D4_SetPushPull()    do { ODCONAbits.ODCA6 = 1; } while(0)
#define D4_SetOpenDrain()   do { ODCONAbits.ODCA6 = 0; } while(0)
#define D4_SetAnalogMode()  do { ANSELAbits.ANSELA6 = 1; } while(0)
#define D4_SetDigitalMode() do { ANSELAbits.ANSELA6 = 0; } while(0)

// get/set D5 aliases
#define D5_TRIS               TRISAbits.TRISA7
#define D5_LAT                LATAbits.LATA7
#define D5_PORT               PORTAbits.RA7
#define D5_WPU                WPUAbits.WPUA7
#define D5_OD                ODCONAbits.ODCA7
#define D5_ANS                ANSELAbits.ANSELA7
#define D5_SetHigh()            do { LATAbits.LATA7 = 1; } while(0)
#define D5_SetLow()             do { LATAbits.LATA7 = 0; } while(0)
#define D5_Toggle()             do { LATAbits.LATA7 = ~LATAbits.LATA7; } while(0)
#define D5_GetValue()           PORTAbits.RA7
#define D5_SetDigitalInput()    do { TRISAbits.TRISA7 = 1; } while(0)
#define D5_SetDigitalOutput()   do { TRISAbits.TRISA7 = 0; } while(0)
#define D5_SetPullup()      do { WPUAbits.WPUA7 = 1; } while(0)
#define D5_ResetPullup()    do { WPUAbits.WPUA7 = 0; } while(0)
#define D5_SetPushPull()    do { ODCONAbits.ODCA7 = 1; } while(0)
#define D5_SetOpenDrain()   do { ODCONAbits.ODCA7 = 0; } while(0)
#define D5_SetAnalogMode()  do { ANSELAbits.ANSELA7 = 1; } while(0)
#define D5_SetDigitalMode() do { ANSELAbits.ANSELA7 = 0; } while(0)

// get/set S1 aliases
#define S1_TRIS               TRISBbits.TRISB4
#define S1_LAT                LATBbits.LATB4
#define S1_PORT               PORTBbits.RB4
#define S1_WPU                WPUBbits.WPUB4
#define S1_OD                ODCONBbits.ODCB4
#define S1_ANS                ANSELBbits.ANSELB4
#define S1_SetHigh()            do { LATBbits.LATB4 = 1; } while(0)
#define S1_SetLow()             do { LATBbits.LATB4 = 0; } while(0)
#define S1_Toggle()             do { LATBbits.LATB4 = ~LATBbits.LATB4; } while(0)
#define S1_GetValue()           PORTBbits.RB4
#define S1_SetDigitalInput()    do { TRISBbits.TRISB4 = 1; } while(0)
#define S1_SetDigitalOutput()   do { TRISBbits.TRISB4 = 0; } while(0)
#define S1_SetPullup()      do { WPUBbits.WPUB4 = 1; } while(0)
#define S1_ResetPullup()    do { WPUBbits.WPUB4 = 0; } while(0)
#define S1_SetPushPull()    do { ODCONBbits.ODCB4 = 1; } while(0)
#define S1_SetOpenDrain()   do { ODCONBbits.ODCB4 = 0; } while(0)
#define S1_SetAnalogMode()  do { ANSELBbits.ANSELB4 = 1; } while(0)
#define S1_SetDigitalMode() do { ANSELBbits.ANSELB4 = 0; } while(0)

// get/set RB7 procedures
#define RB7_SetHigh()    do { LATBbits.LATB7 = 1; } while(0)
#define RB7_SetLow()   do { LATBbits.LATB7 = 0; } while(0)
#define RB7_Toggle()   do { LATBbits.LATB7 = ~LATBbits.LATB7; } while(0)
#define RB7_GetValue()         PORTBbits.RB7
#define RB7_SetDigitalInput()   do { TRISBbits.TRISB7 = 1; } while(0)
#define RB7_SetDigitalOutput()  do { TRISBbits.TRISB7 = 0; } while(0)
#define RB7_SetPullup()     do { WPUBbits.WPUB7 = 1; } while(0)
#define RB7_ResetPullup()   do { WPUBbits.WPUB7 = 0; } while(0)
#define RB7_SetAnalogMode() do { ANSELBbits.ANSELB7 = 1; } while(0)
#define RB7_SetDigitalMode()do { ANSELBbits.ANSELB7 = 0; } while(0)

/**
   @Param
    none
   @Returns
    none
   @Description
    GPIO and peripheral I/O initialization
   @Example
    PIN_MANAGER_Initialize();
 */
void PIN_MANAGER_Initialize (void);

/**
 * @Param
    none
 * @Returns
    none
 * @Description
    Interrupt on Change Handling routine
 * @Example
    PIN_MANAGER_IOC();
 */
void PIN_MANAGER_IOC(void);



#endif // PIN_MANAGER_H
/**
 End of File
*/